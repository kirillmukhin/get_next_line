/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/09 15:01:42 by jarnolf           #+#    #+#             */
/*   Updated: 2020/07/17 15:41:33 by rattlewrench     ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

/*
** Function frees remainder and points dangling pointer to NULL.
** RETURN VALUE
** Returns -1 for cases when it's called because of an error.
*/

static int	free_remainder(char **remainder)
{
	if (*remainder)
		free(*remainder);
	*remainder = NULL;		// Защита от свисающих указателей (dangling pointers), которые указывают на освобожденную память
	return (-1);			// Возвоаем (-1) в случае если вызываем функцию как return при ошибке в главной функции
}

/*
** Function is used to determine return value of get_next_line
** It checks if any error happened during execution,
** or if the end of file has been reached.
** In both of these cases remainder is being freed before the exit.
** RETURN VALUE
** In case of an error, -1 is returned;
** In case of reaching the end of file, 0 is returned.
** In other cases (a line has been read) 1 is returned.
*/

static int	escape_plan(char **remainder, char *n_ptr, int size, int *flag)
{
	if (*flag == -1 || size < 0)				// Где-то произошла ошибка с маллоком (flag = -1) или с чтением (read = -1)
		return (free_remainder(&*remainder));		// Возвращаем (-1), предварительно очистив остаток
	if ((size == 0) && (n_ptr == NULL))			// Дошли до конца файла - нечего читать и в остатке пусто
	{
		free_remainder(&*remainder);
		return (0);
	}
	else										// Если не было ошибок и не дошли до конца файла, значит мы всё-ещё что-то читаем.
		return (1);
}

/*
** Function checks if there's remainder from the previous call.
** If there's remainder, function looks for a newline inside of it, and copies
** all characters up to newline (if found) or till the end of 'remainder'
** to the 'line' array. If newline was found, the reset of the remainder is
** rewritten to the same array; if not than entire 'remainder' has been copied
** to the 'line' and freed afterwards.
** If there's no remainder, 'line' is being initialized.
** RETURN VALUE
** Function returns either NULL if there were no remainder,
** or non-zero value (pointer) if remainder was not empty.
*/

static char	*remainder_check(char **remainder, char **line, int *flag)
{
	char	*n_ptr;

	n_ptr = NULL;	// По умолчанию возвращаем NULL (остатка нет), если остаток находится - указатель меняется на адрес остатка
	if (*remainder != NULL) //	Остаток есть
	{
		n_ptr = ft_gnl_strchr(*remainder);		// Ищем '\n' в остатке, если есть - заменяем первый '\n' на '\0' и получаем адрес, если нет - получаем NULL
		if (!(*line = ft_strdup(*remainder)))	// Копируем остаток до '\0' в line. (Т.е. до конца остатка, или до того места где нашли '\n')
			*flag = free_remainder(&*remainder);	// Обработка потенциальной ошибки маллока
		else 									// Если ошибки не случилось
		{
			if (n_ptr != NULL) 						// '\n' был найден в остатке
				ft_strcpy(*remainder, ++n_ptr);			// копируем всё что шло после '\n' (теперь - после '\0') в начало остатка. (Памяти там более чем достаточно, т.к. строка уже вмещала копируемую часть)
			else									// '\n' не был найден в остатке
				free_remainder(&*remainder);			// освобождаем память
		}
	}
	else //						Остатка нет
		*line = ft_strdup("");		// Инициализируем строку line
	return (n_ptr);
}

/*
** Function reads a line from a given file descriptor 'fd'.
** Read line is recorded in the array, pointed to by 'line'.
** RETURN VALUE
** Function returns 1 when the line has been read, 0 when EOF has been reached,
** and -1 if error happened at any point of execution.
*/

int			get_next_line(int fd, char **line)
{
	static	char	*remainder;
	char			*n_ptr;					// Может быть нулевым значением (NULL) или не-нулевым (адрес, но это не имеет значения). NULL значит что остаток пуст, другое значение - значит что в остатке что-то есть.
	char			buffer[BUFFER_SIZE + 1]; // Размер буффера + 1 знак для конца строки '\0'.
	ssize_t			size;
	int				flag;

	if (fd < 0 || !line || BUFFER_SIZE < 1 || read(fd, buffer, 0) < 0)
		return (-1);
	flag = 0;	// Для отметки о наличии/отсутсвии ошибки в ходе выполнения программы
	size = 0;	// Размер прочитанного буффера, используется для определения EOF и ошибки чтения через read.
	n_ptr = remainder_check(&remainder, line, &flag); // Проверяем остаток с предыдущего вызова
	while (flag == 0 && !n_ptr && (size = read(fd, buffer, BUFFER_SIZE)) > 0) // Если в остатке не было '\n' и не было ошибок (маллока)
	{
		buffer[size] = '\0';	// Гарантированно заканчиваем прочитанный буфер знаком конца строки, место на него есть т.к. задали [BUFFER_SIZE + 1]
		if ((n_ptr = ft_gnl_strchr(buffer)))			// Если нашли '\n' в прочитанном буффере заменяем первый '\n' на '\0'
		{
			free_remainder(&remainder);						// Освобождаем остаток (если он был)
			if (!(remainder = ft_strdup(++n_ptr)))			// Записываем всё что шло после '\n' в остаток remainder
				return (free_remainder(&remainder));			// (-1) в случае ошибки маллока
		}
		if (!(*line = ft_gnl_strjoin(*line, buffer)))	// Присоединяем буффер к 'line'
			return (free_remainder(&remainder));			// (-1) в случае ошибки маллока
	}
	return (escape_plan(&remainder, n_ptr, size, &flag)); // Вызываем функцию, которая вернет нам 1, 0 или -1
}
