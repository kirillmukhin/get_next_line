/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/09 15:00:59 by jarnolf           #+#    #+#             */
/*   Updated: 2020/07/17 15:41:30 by rattlewrench     ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

/*
** Function calculates the length of a string.
*/

size_t	ft_strlen(const char *s)
{
	unsigned int i;

	i = 0;
	if (!s)
		return (0);
	while (s[i] != '\0')
		i++;
	return (i);
}

/*
** Function copies the string pointed to by 'src',
** including the terminating null byte ('\0'),
** to the buffer pointed to by 'dest'.
*/

char	*ft_strcpy(char *dest, const char *src) //копирует строку src в строке dest с гарантированным '\0' в конце
{
	size_t i;

	i = 0;
	if (!dest || !src)
		return (NULL);
	while (src[i] != '\0')
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

/*
** Function duplicates string 's' and returns pointer to the new string.
** Memory for the new string is allocated with malloc.
*/

char	*ft_strdup(const char *s) // Копирует содержание строки 's' в новый массив
{
	char	*dest;

	if (!s)
		return (NULL);
	if (!(dest = (char*)malloc(sizeof(char) * (ft_strlen(s) + 1))))
		return (NULL);
	if (!(ft_strcpy(dest, s)))
		return (NULL);
	return (dest);
}

/*
** Function allocates and returns a fresh string ending
** with ’\0’, result of the concatenation of 'line' and 'buffer'.
** If the allocation fails the function returns NULL.
** Unlike libft variant, 'line' is being freed after successful concatenation.
*/

char	*ft_gnl_strjoin(char *line, char const *buffer) // Соединяет строки line и buffer
{
	char		*res;
	char		*res_ptr;
	size_t		size;
	char		*temp_ptr;

	temp_ptr = line;		// Указатель на полуенный line
	if (!line || !buffer)	// Защита
		return (NULL);
	size = ft_strlen(line) + ft_strlen(buffer); 		// Считаем длину содержимого новой строки
	res = (char *)malloc(sizeof(char) * (size + 1)); 	// Выделяем память на новую строку
	res_ptr = res;										// Запоминаем начало новой строки (Если ошибка маллока, то NULL)
	if (res)											// Если память была выделенна без ошибок:
	{
		while (*line != '\0')							// Копируем весь line в новую строку
			*res++ = *line++;
		while (*buffer)									// Следом, копируем весь buffer в ту же строку
			*res++ = *buffer++;
		*res = '\0';									// Заканчиваем новую строку знаком конца строки '\0'
		free(temp_ptr);									// Освобождаем память в старом line, т.к. он уже скопирован в новую строку
	}
	return (res_ptr);						// Возвращаем указатель на начало новой строки или на NULL если произошла ошибка при выделении памяти.
}

/*
** ft_gnl_strchr function locates character '\n' in the given string 's'.
** If '\n' is found, function replaces it with '\0'
** and returns ponter to that character.
*/

char	*ft_gnl_strchr(char *s) // Ищет знак '\n' в строке 's'
{
	if (!s)
		return (NULL);
	while (*s != '\0')
	{
		if (*s == '\n')	// Ищем знак переноса строки '\n' в строке 's'
		{
			*s = '\0';		// Если нашли - заменяем его на знак конца строки '\0'
			return (s);		// И возвращаем адрес найденного знака
		}
		s++;
	}
	return (NULL);		// Если не нашли - возвращаем NULL
}
