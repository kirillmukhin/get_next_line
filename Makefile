.PHONY: all, clean, re

gnl:
	gcc -g -o gnl -Wall -Wextra -Werror -D BUFFER_SIZE=6 get_next_line.c get_next_line_utils.c main.c

all:	gnl

clean:
	rm -f gnl

re:	clean all
