get_next_line from 42cursus

"This project will not only allow you to add a very convenient function to your collection,
but it will also allow you to learn a highly interesting new concept in C programming:
static variables."

Mandatory part only, no bonuses.
Evaluated with Success 100/100
basic_tests: GNL OK | bonus_tests: GNL KO

Good idea is to check (alongside with basic stuff):
	different BUFFER_SIZE for every case
	empty file (touch text)
	Reading from file redirection: use fd=0 in main and run compiled program like this './a.out < text' where a.out is the name of binary and text is a random text file. It will test reading text file as stdin (like it's typed from a keyboard);
