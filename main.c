/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jarnolf <jarnolf@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 15:13:34 by jarnolf           #+#    #+#             */
/*   Updated: 2020/07/17 14:21:00 by rattlewrench     ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h> //printf
#include <fcntl.h> //open
#include "get_next_line.h"

#ifndef BUFFER_SIZE
# define BUFFER_SIZE 10
#endif

int main(int argc, char **argv)
{
	int             fd;
	int             i;
	int             j;
	char    		*line;

	j = 1;

	if (argc == 1)
		fd = 0;
	else if (argc > 2)
		printf ("Too many arguments");
	else
	{
		char *file = ft_strdup(argv[1]);
		if (!(fd = open(file, O_RDONLY)))
		{
			printf("\nError in open\n");
			return (0);
		}
	}
	printf("\nFD: %d\n", fd);
	if (fd == 0)
		fprintf(stderr, "Press Ctrl+D to mark last line\n");
	while ((i = get_next_line(fd, &line)))
	{
		printf("[gnl: %d] |%d| %s\n", i, j, line);
		free(line);
		j++;
	}
	printf("_-_-_-_-_\n");
	printf("[gnl: %d] |%d| %s\n", i, j, line);;
	free(line);
	close(fd);
	return (0);
}
